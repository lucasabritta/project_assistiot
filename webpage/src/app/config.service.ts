import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpSecurityOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

export interface Config {
  heroesUrl: string;
  textfile: string;
}

function setTemperatura(temperatura_resp) {
  var temperatura = document.getElementById("input_temperatura");
  var temperaturaValue = temperatura_resp["MangOH.Sensors.Pressure.Temperature"][0].value;
  temperaturaValue = parseFloat(temperaturaValue.toString()) - 15;
  temperatura.innerText=temperaturaValue.toFixed(2);;
}
function setLuminosidade(luminosidade_resp) {
  var luminosidade = document.getElementById("input_luminosidade");
  var luminosidadeValue = range(155, 1744, luminosidade_resp["MangOH.Sensors.Light.Level"][0].value)
  luminosidade.innerText=luminosidadeValue.toFixed(2);;
}
function range(limitInf, limitSup, value) {
	parseFloat(value);
	if (value < limitInf) {
		return 0;
	} else if (value > limitSup) {
		return 100;
	} else {
		return (value - 155) / 15.89;
	}
}
function setUmidade(umidade_resp) {
  var umidade = document.getElementById("input_umidade");
  var umidadeValue = umidade_resp["MangOH.Sensors.Pressure.Pressure"][0].value;
  umidadeValue = parseFloat(umidadeValue.toString()) / 2;
  umidade.innerText=umidadeValue.toFixed(2);;
}

@Injectable()
export class ConfigService {
  temperatureUrl = 'https://eu.airvantage.net/api/v1/systems/174267e8391147b39aea99c51c2d4976/data?ids=MangOH.Sensors.Pressure.Temperature';
  luminosidadeUrl = 'https://eu.airvantage.net/api/v1/systems/174267e8391147b39aea99c51c2d4976/data?ids=MangOH.Sensors.Light.Level';
  umidadeUrl = 'https://eu.airvantage.net/api/v1/systems/174267e8391147b39aea99c51c2d4976/data?ids=MangOH.Sensors.Pressure.Pressure';
  securityUrl = 'https://eu.airvantage.net/api/oauth/token';
  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer 1c74f00b-a7ac-4624-aeda-01e9fc87a07a'
    })
  };
  commandBody = JSON.parse('{"systems" : {"uids": ["174267e8391147b39aea99c51c2d4976"]},"commandId" : "redSensorToCloud/SetLedBlinkInterval","parameters" : {"LedBlinkInterval": "0"}}');
  commandUrl = "https://eu.airvantage.net/api/v1/operations/systems/command";

  constructor(private http: HttpClient) {
    this.checkToken();
   }

  checkToken() {
    this.http.get(this.temperatureUrl, this.httpOptions)
    .subscribe(data => {return data}, // success path
               error => this.handleNoToken(error) // error path
      );
  }

  handleNoToken(error) {
    if (error.status == 401) {
      this.setSecurityToken();
    }
  }

  getTemperature() {
    return this.http.get(this.temperatureUrl, this.httpOptions)
    .subscribe(data => {setTemperatura(data)});
  }
  getLuminosidade() {
    return this.http.get(this.luminosidadeUrl, this.httpOptions)
    .subscribe(data => {setLuminosidade(data)});
  }
  getUmidade() {
    return this.http.get(this.umidadeUrl, this.httpOptions)
    .subscribe(data => {setUmidade(data)});
  }

  alterarValor(LedBlinkInterval) {
    this.commandBody.parameters.LedBlinkInterval = LedBlinkInterval;
    this.http.post(this.commandUrl, this.commandBody, this.httpOptions)
    .subscribe(data => {});
  }
  
  setSecurityToken() {
    var securityBody = new URLSearchParams();
    securityBody.set('grant_type', 'password');
    securityBody.set('username', 'cmtandrade2002@gmail.com');
    securityBody.set('password', 'Dancaioairvantage_2018');
    securityBody.set('client_id', '0a71f97e04184f6b8a1dbbd2772f8215');
    securityBody.set('client_secret', 'aa6acf566f2f4aab85714fa66da7de36');
    return this.http.post(this.securityUrl, securityBody.toString(), httpSecurityOptions)
    .subscribe(data => {this.httpOptions.headers = new HttpHeaders({
        'Authorization': 'Bearer ' + data["access_token"].toString()
      }) 
    });
  }
}