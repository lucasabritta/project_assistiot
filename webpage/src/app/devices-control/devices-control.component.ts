import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/config.service';

@Component({
  selector: 'app-devices-control',
  templateUrl: './devices-control.component.html',
  providers: [ ConfigService ],
  styleUrls: ['./devices-control.component.css']
})
export class DevicesControlComponent {

  constructor(public configService: ConfigService) { }

  alterarValor(select_id) {
    var LedBlinkInterval = document.getElementById(select_id)["value"];
    this.configService.alterarValor(LedBlinkInterval);
  }
}