import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicesControlComponent } from './devices-control.component';

describe('DevicesControlComponent', () => {
  let component: DevicesControlComponent;
  let fixture: ComponentFixture<DevicesControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicesControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicesControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
