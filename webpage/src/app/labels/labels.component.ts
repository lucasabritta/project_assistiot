import { Component  } from '@angular/core';
import { ConfigService } from 'src/app/config.service';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  providers: [ ConfigService ],
  styleUrls: ['./labels.component.css']
})

export class LabelsComponent {
  constructor(public configService: ConfigService) {}

  showValues() {
    this.configService.getTemperature();
    this.configService.getLuminosidade();
    this.configService.getUmidade();
  }
}