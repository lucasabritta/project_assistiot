import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WebCamComponent } from './web-cam/web-cam.component';
import { LabelsComponent } from './labels/labels.component';
import { MovementControlComponent } from './movement-control/movement-control.component';
import { DevicesControlComponent } from './devices-control/devices-control.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    WebCamComponent,
    LabelsComponent,
    MovementControlComponent,
    DevicesControlComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
