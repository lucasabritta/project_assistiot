import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementControlComponent } from './movement-control.component';

describe('MovementControlComponent', () => {
  let component: MovementControlComponent;
  let fixture: ComponentFixture<MovementControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
