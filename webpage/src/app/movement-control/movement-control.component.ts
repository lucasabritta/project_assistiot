import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/config.service';

@Component({
  selector: 'app-movement-control',
  templateUrl: './movement-control.component.html',
  providers: [ ConfigService ],
  styleUrls: ['./movement-control.component.css']
})
export class MovementControlComponent {

  constructor(public configService: ConfigService) { }

  alterarValor(select_id) {
    this.configService.alterarValor(select_id);
  }
}
