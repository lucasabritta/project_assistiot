/*
 * AUTO-GENERATED interface.h for the avPublisherComponent component.

 * Don't bother hand-editing this file.
 */

#ifndef __avPublisherComponent_COMPONENT_INTERFACE_H_INCLUDE_GUARD
#define __avPublisherComponent_COMPONENT_INTERFACE_H_INCLUDE_GUARD

#ifdef __cplusplus
extern "C" {
#endif

#include "le_gpioPin22_interface.h"
#include "le_gpioPin23_interface.h"
#include "le_gpioPin24_interface.h"
#include "le_gpioPin35_interface.h"
#include "le_avdata_interface.h"
#include "ma_led_interface.h"

#ifdef __cplusplus
}
#endif

#endif // __avPublisherComponent_COMPONENT_INTERFACE_H_INCLUDE_GUARD
